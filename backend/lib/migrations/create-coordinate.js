module.exports = {
    up: function(queryInterface, Sequelize) {
        return queryInterface.createTable('coordinates', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            trackerId: {
                allowNull: false,
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            lat: Sequelize.STRING,
            lng: Sequelize.STRING,
            dateTime: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },

    down: function(queryInterface) {
        return queryInterface.dropTable('coordinates');
    }
}