import express from 'express';
import coordinates from './coordinates'

const router = express.Router();
router.use(coordinates);
export default router;