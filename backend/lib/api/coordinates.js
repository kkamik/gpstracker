import express from 'express';
import models from '../models';
import logger from '../logger';

const router = express.Router();

//all list
router.get('/coordinates/', function(req, res) {
    models.coordinates.findAll({ attributes: ['id', 'lat', 'lng','dateTime', 'trackerId']})
    .then(function(coordinates) {
        res.json(coordinates.map(coord => coord.toJSON()));
    })
    .catch(function(e) {
        logger.error('error fetching coordinates', e);
        res.status(500).json({});
    });
});

//delete
router.delete('/coordinates/:id/', function(req, res) {
    models.coordinates.findById(req.params.id)
    .then(function(coord) {
        if (coord) {
            return coord.destroy()
            .then(function() {
                res.status(200).json({});
            });
        }

        res.status(404).json({});
    })
    .catch(function(err) {
        logger.error('error destroying coord', err);
        res.status(500).json({});
    });
});

// create
router.post('/coordinates/', function(req, res) {
    logger.info('creating coordinate', JSON.stringify(req.body));

    models.coordinates.create(req.body)
    .then(function(coordinate) {
        res.status(201).json(coordinate.toJSON());
    })
    .catch(function(err) {
        logger.error('error creating coordinate', err);
        res.status(500).json();
    });
});

export default router;