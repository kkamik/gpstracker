export default function(sequelize, DataTypes) {
    const Coordinates = sequelize.define("coordinates", {
        lat: DataTypes.STRING,
        lng: DataTypes.STRING,
        dateTime: DataTypes.DATE,
        trackerId: DataTypes.STRING
    });

    return Coordinates;
};