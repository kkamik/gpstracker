import baseConfig from './webpack.config.base';
import webpack from 'webpack';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import AssetsWebpackPlugin from 'assets-webpack-plugin';

export default {
    ...baseConfig,

    output: {
        ...baseConfig.output,
        filename: '[hash]_bundle.js'
    },

    module: {
        ...baseConfig.module,

        // wrap style loaders with extract text plugin
        loaders: baseConfig.module.loaders.map(conf => {
            const confLoader = conf.loader && conf.loader.includes('style!') ?
                ExtractTextPlugin.extract('style', conf.loader.replace('style!', '')) :
                conf.loader;

            return {
                ...conf,
                confLoader
            };
        })
    },

    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            children: true,
            async: true
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.DefinePlugin({
            '__DEV__': false,
            'process.env.NODE_ENV': JSON.stringify('production'),
            'process.env': JSON.stringify('production')
        }),
        new webpack.optimize.UglifyJsPlugin({
            beautify: false,
            comments: false,
            compress: {
                screw_ie8: true,
                sequences: true,
                comparisons: true,
                conditionals: true,
                evaluate: true,
                booleans: true,
                loops: true,
                unused: true,
                hoist_funs: true,
                if_return: true,
                join_vars: true,
                cascade: true,
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        }),
        new ExtractTextPlugin('[hash]_styles.css'),
        new AssetsWebpackPlugin({ filename: 'dist/assets.json' })
    ]
};
