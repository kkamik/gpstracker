import baseConfig from './webpack.config.base';
import webpack from 'webpack';

export default {
    ...baseConfig,
    debug: true,
    output: {
        ...baseConfig.output,
        publicPath: `http://localhost:3000${baseConfig.output.publicPath}`,
        filename: 'bundle.js'
    },
    entry: [
        'webpack-dev-server/client?http://0.0.0.0:3000',
        'webpack/hot/only-dev-server',
        ...baseConfig.entry
    ],
    plugins: [
        ...baseConfig.plugins,
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            '__DEV__': true,
            'process.env': JSON.stringify('development')
        })
    ]
};
