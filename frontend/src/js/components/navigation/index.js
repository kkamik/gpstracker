import React from 'react';
import { Link } from 'react-router';

const navItems = [
    {
        link: '/home',
        text: 'Home'
    },
    {
        link: '/coordinations',
        text: 'Add coordination'
    },
    {
        link: '/location',
        text: 'Map'
    }
];

const Navigation = () => (
    <header className='container'>
        <ul className='btn-group btn-group-justified'>
            { navItems.map((object, i) =>
                <div className='btn-group' key={i}>
                    <Link
                        to={object.link}
                        className='btn btn-primary'
                        key={i}
                    >
                        { object.text }
                    </Link>
                </div>
            )}
        </ul>
    </header>
);

export default Navigation;
