import React from 'react';

const Marker = (props =>
    <div className='marker-wrapper'>
        <i className='glyphicon glyphicon-map-marker' />
        <p className='market-text'>{props.dateTime}</p>
    </div>
);

Marker.propTypes = {
    dateTime: React.PropTypes.string
};

export default Marker;
