import React from 'react';
import moment from 'moment';

const CoordList = (props =>
    <table className='table table-hover'>
        <thead>
            <tr>
                <th>Tracker ID</th>
                <th>Latitude</th>
                <th>Longtitude</th>
                <th colSpan='2'>Date & Time</th>
            </tr>
        </thead>
        <tbody>
            {
                props.coordinates.map((coord, index) =>
                    <tr key={index}>
                        <td>{coord.trackerId}</td>
                        <td>{coord.lat}</td>
                        <td>{coord.lng}</td>
                        <td>{new moment(coord.dateTime).format('DD-MM-YYYY HH:mm')}</td>
                        <td>
                            <button className='btn btn-danger' onClick={() => props.delete(coord.id)}>delete</button>
                        </td>
                    </tr>
                )
            }
        </tbody>
    </table>
);

CoordList.propTypes = {
    coordinates: React.PropTypes.array
};

export default CoordList;
