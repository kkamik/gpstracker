import React, { PropTypes as pt } from 'react';

const InputWithGlyphicon = props => {
    const infoGlyphicon = props.error ?
        <span className='glyphicon glyphicon glyphicon-ok' /> :
        <span className='glyphicon glyphicon-exclamation-sign' />;

    return (
        <div className='col-xs-4 input-wrapper'>
            <input
                className='form-control check'
                placeholder={props.placeholder}
                value={props.value}
                onChange={props.handleInputCoord(props.essence)}
            />
            { infoGlyphicon }
        </div>
    );
};

export default InputWithGlyphicon;


InputWithGlyphicon.propTypes = {
    value: pt.string,
    error: pt.bool,
    placeholder: pt.string,
    essence: pt.string,
    handleInputCoord: pt.func
};
