import types from '../actions/types';

const initialState = [];

export default function(state = initialState, action = {}) {
    switch (action.type) {
        case types.FETCH_COORDS.SUCCESS:
            return action.payload;
        case types.CREATE_COORDS.SUCCESS:
            return [...state, action.payload];
        case types.DELETE_COORDS.REQUEST:
            return state.filter(coordinate => coordinate.id !== action.payload.id);
        default:
            return state;
    }
}
