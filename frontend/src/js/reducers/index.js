import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import coordinates from './coordinates';

export default combineReducers({
    routing: routerReducer,
    coordinates
});
