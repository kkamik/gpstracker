import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../style/index.styl';

import configureStore from './configureStore';

import AppContainer from './containers/AppContainer';
import HomeContainer from './containers/HomeContainer';
import CoordContainer from './containers/CoordContainer';
import GmapsContainer from './containers/GmapsContainer';

const store = configureStore();

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route path='/' component={AppContainer} >
                <Route path='home' component={HomeContainer} />
                <Route path='coordinations' component={CoordContainer} />
                <Route path='location' component={GmapsContainer} />
            </Route>
        </Router>
    </Provider>,
    document.getElementById('mount')
);
