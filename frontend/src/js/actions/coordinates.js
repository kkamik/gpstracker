import { CALL_API } from 'redux-api-middleware';
import types from './types';


export function fetchCoordinates(filter) {
    return {
        [CALL_API]: {
            endpoint: '/api/coordinates/',
            method: 'GET',
            types: types.FETCH_COORDS.all
        }
    };
}

export function deleteCoordinate(id) {
    return {
        [CALL_API]: {
            endpoint: `/api/coordinates/${id}/`,
            method: 'DELETE',
            types: [
                {
                    type: types.DELETE_COORDS.REQUEST,
                    payload: { id }
                },
                types.DELETE_COORDS.SUCCESS,
                types.DELETE_COORDS.FAILURE
            ]
        }
    };
}

export function createCoordinate(data) {
    return {
        [CALL_API]: {
            endpoint: '/api/coordinates/',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
            method: 'POST',
            types: types.CREATE_COORDS.all
        }
    };
}
