const types = {
    FETCH_COORDS: 'async',
    CREATE_COORDS: 'async',
    DELETE_COORDS: 'async'
};

/*
transforms each value:

{
    FOO: null,
    BAR: 'async'
}

becomes

{
    FOO: 'FOO',
    BAR: {
        REQUEST: 'BAR_REQUEST',
        SUCCESS: 'BAR_SUCCESS',
        FAILURE: 'BAR_FAILURE',
        all: ['BAR_REQUEST' 'BAR_SUCCESS', 'BAR_FAILURE']
    }
}

*/


Object.keys(types).forEach(key => {
    if (types[key] === 'async') {
        types[key] = {
            all: []
        };
        ['REQUEST', 'SUCCESS', 'FAILURE'].forEach(type => {
            const fullType = `${key}_${type}`;

            types[key][type] = fullType;
            types[key].all.push(fullType);
        });
    } else if (types[key] === null) {
        types[key] = key;
    } else {
        throw new Error('type value must be "async" or null');
    }
});

export default types;
