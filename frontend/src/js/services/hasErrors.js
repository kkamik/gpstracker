
const hasErrors = errors => Object.values(errors).filter(value => !value).length;

export default hasErrors;
