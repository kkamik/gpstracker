import React from 'react';
import Navigation from '../components/navigation';
import HomeContainer from './HomeContainer';

const AppContainer = ({ children }) => (
    <div className='app'>
        <Navigation />
        <main>
            {children || <HomeContainer /> }
        </main>
    </div>
);

AppContainer.propTypes = {
    children: React.PropTypes.node
};

export default AppContainer;
