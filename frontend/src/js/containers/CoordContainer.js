import React, { Component, PropTypes as pt } from 'react';
import { connect } from 'react-redux';
import { fetchCoordinates, createCoordinate, deleteCoordinate } from '../actions/coordinates';
import CoordList from '../components/coordinates/CoordList';
import InputWithGlyphicon from '../components/coordinates/InputWithGlyphicon';
import hasErrors from '../services/hasErrors';


class CoordContainer extends Component {

    static propTypes = {
        fetchCoordinates: pt.func,
        createCoordinate: pt.func,
        deleteCoordinate: pt.func,
        coordinates: pt.array
    }

    constructor(props) {
        super(props);

        this.state = {
            trackerId: '',
            lat: '',
            lng: '',
            errors: {
                lat: false,
                lng: false,
                trackerId: false
            }
        };
    }

    componentDidMount() {
        this.props.fetchCoordinates();
    }

    onIdChange = type => ({ target: { value } }) => {
        this.setState(state => {
            state[type] = value;
            state.errors[type] = !!value;
            return state;
        });
    };

    onInputCoord = type => ({ target: { value } }) => {
        const coordPattern = /^-{0,1}\d{2}.\d{5,}/;
        const isCoordValid = coordPattern.test(value);

        this.setState(state => {
            state[type] = value;
            state.errors[type] = isCoordValid;
            return state;
        });
    };

    handleClickCreateButton = () => {
        this.setState({
            lat: '',
            lng: '',
            errors: {
                lat: false,
                lng: false
            }
        });
        return this.props.createCoordinate({
            trackerId: this.state.trackerId,
            lat: this.state.lat,
            lng: this.state.lng,
            dateTime: new Date()
        });
    };

    render() {
        return (
            <div className='container'>
                <div className='inputs-wrapper'>
                    <div className='col-xs-4 input-wrapper'>
                        <input
                            className='form-control'
                            placeholder='Enter tracker id'
                            value={this.state.trackerId}
                            onChange={this.onIdChange('trackerId')}
                        />
                    </div>
                    <InputWithGlyphicon
                        placeholder='Enter latitude'
                        essence='lat'
                        value={this.state.lat}
                        error={this.state.errors.lat}
                        handleInputCoord={this.onInputCoord}
                    />
                    <InputWithGlyphicon
                        placeholder='Enter longitude'
                        essence='lng'
                        value={this.state.lng}
                        error={this.state.errors.lng}
                        handleInputCoord={this.onInputCoord}
                    />
                    <div className='col-xs-12'>
                        <button
                            className='btn btn-info text-center center-block add-coord-button'
                            disabled={hasErrors(this.state.errors)}
                            onClick={this.handleClickCreateButton}
                        >
                            Add coordinate
                        </button>
                    </div>
                </div>
                <CoordList delete={this.props.deleteCoordinate} coordinates={this.props.coordinates} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    coordinates: state.coordinates
});

const mapDispatchToProps = {
    fetchCoordinates,
    createCoordinate,
    deleteCoordinate
};

export default connect(mapStateToProps, mapDispatchToProps)(CoordContainer);
