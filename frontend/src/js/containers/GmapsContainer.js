import React, { Component, PropTypes as pt } from 'react';
import moment from 'moment';
import Map from 'google-map-react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.min.css';
import { connect } from 'react-redux';
import { fetchCoordinates } from '../actions/coordinates';
import Marker from '../components/gmaps/Marker';

class GmapsContainer extends Component {
    static defaultProps = {
        center: { lat: 50, lng: 30 },
        zoom: 5
    };

    static propTypes = {
        center: pt.object,
        zoom: pt.number,
        fetchCoordinates: pt.func,
        coordinates: pt.array
    };

    constructor(props) {
        super(props);

        this.state = {
            trackerId: '',
            startDate: null,
            endDate: null
        };
    }

    componentDidMount() {
        this.props.fetchCoordinates();
        this.isMount = true;
    }

    componentWillUnmount() {
        this.isMount = false;
    }

    getAllPoints() {
        return this.props.coordinates.map(item => {
            const formatedDateTime = new moment(item.dateTime).format('D MMM-HH:mm');

            return (
                <Marker
                    key={item.id}
                    lat={item.lat}
                    lng={item.lng}
                    dateTime={formatedDateTime}
                />
            );
        });
    }

    handleTrackerId = ({ target: { value } }) => {
        this.setState({
            trackerId: value
        }, this.updateByFilters);
    };

    handleChangeDate = type => date => {
        this.setState({
            [type]: date
        }, this.updateByFilters);
    };

    createMapOptions = () => ({
        panControl: false,
        mapTypeControl: true,
        scrollwheel: true
    });

    updateByFilters = () => {
        const { trackerId, startDate, endDate } = this.state;

        this.props.fetchCoordinates({
            trackerId,
            startDate,
            endDate
        });
    };

    isMount = false;

    render() {
        return (
            <div className='gmaps-wrapper'>
                <div className='inputs-wrapper container'>
                    <div className='col-xs-4 input-wrapper'>
                        <input
                            className='form-control'
                            type='text'
                            onChange={this.handleTrackerId}
                            placeholder='Enter tracker id'
                        />
                    </div>

                    <div className='col-xs-4 input-wrapper'>
                        <DatePicker
                            dateFormat='DD.MM.YYYY'
                            className='form-control'
                            selected={this.state.startDate}
                            selectsStart
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            onChange={this.handleChangeDate('startDate')}
                            placeholderText='Select the start date'
                            isClearable
                        />
                    </div>

                    <div className='col-xs-4 input-wrapper'>
                        <DatePicker
                            dateFormat='DD.MM.YYYY'
                            className='form-control'
                            selected={this.state.endDate}
                            selectsEnd
                            startDate={this.state.startDate}
                            endDate={this.state.endDate}
                            onChange={this.handleChangeDate('endDate')}
                            placeholderText='Select the end date'
                            isClearable
                        />
                    </div>
                </div>

                <div className='map-inner'>
                    <Map
                        defaultCenter={this.props.center}
                        defaultZoom={this.props.zoom}
                        onReady={this.updateByFilters}
                        options={this.createMapOptions}
                    >
                        { this.getAllPoints() }
                    </Map>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    coordinates: state.coordinates
});

const mapDispatchToProps = {
    fetchCoordinates
};

export default connect(mapStateToProps, mapDispatchToProps)(GmapsContainer);

