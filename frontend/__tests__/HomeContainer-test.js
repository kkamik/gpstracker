import React from 'react';
import { shallow } from 'enzyme';
import HomeContainer from '../src/js/containers/HomeContainer';

test('HomeContainer text', () => {
    const container = shallow(
        <HomeContainer />
    );

    expect(container.text()).toEqual('Welcome my friend');
});
